require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || '127.0.0.1',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || '127.0.0.1',
  apiPort: process.env.APIPORT,
  app: {
    title: 'Tasks.trial',
    description: 'All the modern best practices.',
    head: {
      titleTemplate: 'Tasks: %s',
      meta: [
        {name: 'description', content: 'All the modern best practices.'},
        {charset: 'utf-8'}
      ]
    }
  }

}, environment);
