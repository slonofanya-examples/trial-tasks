import {createValidator, required} from 'utils/validation';

const widgetValidation = createValidator({
  description: required
});

export default widgetValidation;
