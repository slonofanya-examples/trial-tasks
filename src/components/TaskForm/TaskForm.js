import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import lodash from 'lodash';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form';
import { Row, Col } from 'react-bootstrap';
import taskValidation from './validation';
import * as taskActions from 'redux/modules/tasks';

@connect(
  state => ({
    saveError: state.tasks.saveError
  }),
  dispatch => bindActionCreators(taskActions, dispatch)
)

@reduxForm({
  form: 'task',
  fields: ['description', 'assigned', 'state'],
  validate: taskValidation
})

export default class TaskForm extends Component {
  static propTypes = {
    users: PropTypes.array,
    fields: PropTypes.object.isRequired,
    editStop: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    save: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    saveError: PropTypes.object,
    formKey: PropTypes.string.isRequired,
    values: PropTypes.object.isRequired,
    onSubmit: PropTypes.func,
    onCancel: PropTypes.func
  };

  cancelHandle() {
    const { editStop, formKey, onCancel } = this.props;
    onCancel();
    return editStop(formKey);
  }

  render() {
    const { fields: {description, assigned, state}, formKey, handleSubmit, invalid,
      submitting, saveError: { [formKey]: saveError },
      users
      } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <input type="description" className="form-control" {...description}/>
          {description.error && description.touched && <div className="text-danger">{description.error}</div>}
        </div>

        <div className="form-group">
          <select name="assigned" className="form-control" {...assigned}>
            {lodash.map(users, user =>
              <option value={user}
                      key={`user_${user}`}>
                {user}
              </option>)}
          </select>
          {assigned.error && assigned.touched && <div className="text-danger">{assigned.error}</div>}
        </div>

        <div className="form-group">
          <select name="state" className="form-control" {...state} disabled={!state.value}>
            {lodash.map(['open', 'done'], stateName =>
              <option value={stateName}
                      key={`state_${stateName}`}>
                {stateName}
              </option>)}
          </select>
          {state.error && state.touched && <div className="text-danger">{state.error}</div>}
        </div>

        <Row>
          <Col md={12}>
            <button onClick={this.cancelHandle.bind(this)}
                    disabled={!!submitting}
                    className="pull-left btn">
              <i className="fa fa-ban"/> Cancel
            </button>

            <button type="submit"
                    disabled={!!(description.error || invalid || submitting)}
                    className="pull-right btn btn-primary">
              <i className={'fa ' + (submitting ? 'fa-cog fa-spin' : 'fa-cloud')}/> Save
            </button>

            {saveError && <div className="text-danger">{saveError}</div>}
          </Col>
        </Row>
      </form>
    );
  }
}
