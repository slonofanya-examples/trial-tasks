import lodash from 'lodash';

const LOAD = 'redux-example/tasks/LOAD';
const LOAD_SUCCESS = 'redux-example/tasks/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/tasks/LOAD_FAIL';
const EDIT_START = 'redux-example/tasks/EDIT_START';
const EDIT_STOP = 'redux-example/tasks/EDIT_STOP';
const SAVE = 'redux-example/tasks/SAVE';
const SAVE_SUCCESS = 'redux-example/tasks/SAVE_SUCCESS';
const SAVE_FAIL = 'redux-example/tasks/SAVE_FAIL';
const DELETE = 'redux-example/tasks/DELETE';
const DELETE_SUCCESS = 'redux-example/tasks/DELETE_SUCCESS';
const DELETE_FAIL = 'redux-example/tasks/DELETE_FAIL';

const initialState = {
  loaded: false,
  editing: {},
  saveError: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result || [],
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        error: action.error
      };
    case EDIT_START:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: true
        }
      };
    case EDIT_STOP:
      return {
        ...state,
        editing: {
          ...state.editing,
          [action.id]: false
        }
      };
    case SAVE:
      return state; // 'saving' flag handled by redux-form
    case SAVE_SUCCESS:
      const data = [...state.data];

      const index = lodash.findIndex(data, row => row.id === action.result.id);
      if (data[index]) {
        data[index] = action.result;
      } else {
        data.push(action.result);
      }

      return {
        ...state,
        data,
        editing: {
          ...state.editing,
          [action.id]: false
        },
        saveError: {
          ...state.saveError,
          [action.id]: null
        }
      };
    case SAVE_FAIL:
      return typeof action.error === 'string' ? {
        ...state,
        saveError: {
          ...state.saveError,
          [action.id]: action.error
        }
      } : state;
    case DELETE:
      return {
        ...state,
        loading: true
      };
    case DELETE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: lodash.filter([...state.data], row => row.id !== +action.result.delete),
        error: null
      };
    case DELETE_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: [],
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.tasks && globalState.tasks.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/tasks') // params not used, just shown as demonstration
  };
}

export function save(data) {
  return {
    types: [SAVE, SAVE_SUCCESS, SAVE_FAIL],
    promise: (client) => client.post('/tasks', { data })
  };
}

export function deleteTask(id) {
  return {
    types: [DELETE, DELETE_SUCCESS, DELETE_FAIL],
    promise: (client) => client.del('/tasks', { params: { id } })
  };
}

export function editStart(id) {
  return { type: EDIT_START, id };
}

export function editStop(id) {
  return { type: EDIT_STOP, id };
}
