import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-async-connect';
import * as taskActions from 'redux/modules/tasks';
import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import { isLoaded as isTasksLoaded, load as loadTasks } from 'redux/modules/tasks';
import { isLoaded as isUsersLoaded, load as loadUsers } from 'redux/modules/users';
import Helmet from 'react-helmet';
import lodash from 'lodash';
import { Tasks, EditTask } from 'containers';
const styles = require('./Home.scss');

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }

    if (!isTasksLoaded(getState())) {
      promises.push(dispatch(loadTasks()));
    }

    if (!isUsersLoaded(getState())) {
      promises.push(dispatch(loadUsers()));
    }

    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    user: state.auth.user,
    tasks: state.tasks.data,
    error: state.tasks.error
  }),
  { ...taskActions }
)

export default class Home extends Component {
  static propTypes = {
    user: PropTypes.object,
    tasks: PropTypes.array,
    error: PropTypes.object,
    load: PropTypes.func,
    save: PropTypes.func,
    deleteTask: PropTypes.func
  };

  componentWillMount() {
    this.state = {
      editTask: null
    };

    if (__CLIENT__ && socket) {
      socket.on('changedTaskList', this.handleServerUpdated.bind(this));
    }
  }

  handleServerUpdated() {
    this.props.load();
  }

  handleEdit(task) {
    lodash.assign(this.state, {
      editTask: task
    });
    this.forceUpdate();
  }

  handleDelete(task) {
    this.props.deleteTask(task.id).then(() => {
      this.state.editTask = null;
      this.forceUpdate();
      socket.emit('changedTaskList', {});
    });
  }

  handleSave(task) {
    const saveTask = lodash.assign({}, this.state.editTask, task);

    this.props.save(saveTask).then(() => {
      this.state.editTask = null;
      this.forceUpdate();
      socket.emit('changedTaskList', {});
    });
  }

  handleCancelEdit() {
    this.state.editTask = null;
    this.forceUpdate();
  }

  render() {
    const { user, tasks } = this.props;
    const { editTask } = this.state;

    return (
      <div className={styles.home}>
        <Helmet title="Home"/>

        <div className="container">
          {user && (<Tasks
            user={user}
            tasks={tasks}
            onEdit={this.handleEdit.bind(this)}
            onDelete={this.handleDelete.bind(this)}
          />)}

          {editTask &&
          <EditTask task={editTask}
                    onSave={this.handleSave.bind(this)}
                    onCancel={this.handleCancelEdit.bind(this)}/>}
        </div>
      </div>
    );
  }
}
