import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-async-connect';
import lodash from 'lodash';
import moment from 'moment';
import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import { Button } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }

    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    user: state.auth.user
  }), { }
)

export default class Tasks extends Component {
  static propTypes = {
    user: PropTypes.object,
    tasks: PropTypes.array,
    onEdit: PropTypes.func,
    onDelete: PropTypes.func
  };

  render() {
    const { user, tasks, onEdit, onDelete} = this.props;

    const rows = lodash.map(tasks, (task) =>
      lodash.assign({}, task, {
        creationDate: moment(task.creationDate).format(),
        actions: __CLIENT__ ? (<div>
          <Button onClick={onEdit.bind(null, task)}>Edit</Button>
          &nbsp;
          {task.owner === user.name &&
          <Button onClick={onDelete.bind(null, task)}>Delete</Button>}
        </div>) : ''
      }));

    return (
      <div>
        <br/>
        <div>
          <Button onClick={onEdit.bind(null, {})}>Create</Button>
        </div>
        <br/>

        <BootstrapTable data={rows} striped={!0} hover={!0}>
          <TableHeaderColumn dataField="creationDate" dataAlign="center" dataSort={!0}>Creation date</TableHeaderColumn>
          <TableHeaderColumn dataField="description" dataAlign="center" dataSort={!0}
                             isKey={!0}>Description</TableHeaderColumn>
          <TableHeaderColumn dataField="owner" dataAlign="center" dataSort={!0}>Owner</TableHeaderColumn>
          <TableHeaderColumn dataField="assigned" dataAlign="center" dataSort={!0}>Assigned</TableHeaderColumn>
          <TableHeaderColumn dataField="state" dataAlign="center" dataSort={!0}>State</TableHeaderColumn>
          <TableHeaderColumn dataField="actions" dataAlign="center" dataSort={!!0}>Actions</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
