import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import lodash from 'lodash';
import * as taskActions from 'redux/modules/tasks';
import { Modal } from 'react-bootstrap';
import { initializeWithKey } from 'redux-form';
import { TaskForm } from 'components';

@connect(
  state => ({
    user: state.auth.user,
    users: state.users.data,
    editing: state.tasks.editing,
    error: state.tasks.error,
    loading: state.tasks.loading
  }),
  { ...taskActions, initializeWithKey }
)

export default class EditTask extends Component {
  static propTypes = {
    users: PropTypes.array,
    task: PropTypes.object,
    onSave: PropTypes.func,
    onCancel: PropTypes.func,
    initializeWithKey: PropTypes.func.isRequired
  };

  render() {
    const { task, users, onSave, onCancel } = this.props;

    return (
      <Modal bsSize="small" show={!0} onHide={onCancel}>
        <Modal.Header closeButton>
          <Modal.Title>{task.id ? 'Edit task' : 'Create task'}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <TaskForm users={users}
                    formKey={String(task.id)}
                    key={String(task.id)}
                    initialValues={lodash.assign({ assigned: '', status: 'open' }, task)}
                    onSubmit={onSave}
                    onCancel={onCancel}/>
        </Modal.Body>
      </Modal>
    );
  }
}
