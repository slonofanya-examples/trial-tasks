import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { IndexLink } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { NavItem, Nav, Navbar, Modal } from 'react-bootstrap/lib';
import Helmet from 'react-helmet';
import { isLoaded as isAuthLoaded, load as loadAuth, login, logout } from 'redux/modules/auth';
import { load as loadTasks } from 'redux/modules/tasks';
import { push } from 'react-router-redux';
import config from '../../config';
import { asyncConnect } from 'redux-async-connect';
const styles = require('./App.scss');

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }

    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    user: state.auth.user,
    loading: state.tasks.loading
  }),
  { login, logout, pushState: push, loadTasks }
)

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    user: PropTypes.object,
    login: PropTypes.func,
    loadTasks: PropTypes.func,
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired,
    loading: PropTypes.bool
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillMount() {
    this.state = {
      alert: false
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const usernameInput = ReactDOM.findDOMNode(this.username);
    const passwordInput = ReactDOM.findDOMNode(this.password);

    this.props.login(usernameInput.value, passwordInput.value)
      .then(() => {
        return this.props.loadTasks();
      })
      .catch(err => {
        this.state = {
          alert: {
            message: err.error
          }
        };
        this.forceUpdate();
      });

    usernameInput.value = '';
    passwordInput.value = '';
  };

  handleLogout = (event) => {
    event.preventDefault();
    this.props.logout();
  };

  alertClose() {
    this.state = {
      alert: false
    };
    this.forceUpdate();
  }

  render() {
    const { user, loading } = this.props;
    const {alert} = this.state;

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>
        <Navbar fixedTop>
          <Navbar.Header>
            <Navbar.Brand>
              <IndexLink to="/" activeStyle={{color: '#33e0ff'}}>
                <div className={styles.brand}/>
                <span>{config.app.title}</span>
              </IndexLink>
            </Navbar.Brand>
            <Navbar.Toggle/>
          </Navbar.Header>

          <Navbar.Collapse eventKey={0}>
            {!user && (
              <Navbar.Collapse eventKey={1}>
                <Navbar.Form>
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <input type="text"
                             ref={(ref) => this.username = ref}
                             placeholder="Enter a username"
                             required
                             id="username"
                             className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <input type="text"
                             ref={(ref) => this.password = ref}
                             placeholder="Enter a password"
                             required
                             id="password"
                             className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <button type="submit" className="btn btn-primary login-btn">Log In</button>
                    </div>
                  </form>
                </Navbar.Form>
              </Navbar.Collapse>
            )}

            {user &&
            <p className={styles.loggedInMessage + ' navbar-text username'}>
              Logged in as <strong>{user.name}</strong>.
            </p>}

            {loading && (
              <Navbar.Text>
                <i className="fa fa-refresh fa-spin"></i>
              </Navbar.Text>
            )}

            <Nav navbar pullRight>
              {user &&
              <LinkContainer to="/logout">
                <NavItem eventKey={2} className="logout-link" onClick={this.handleLogout}>
                  Logout
                </NavItem>
              </LinkContainer>}
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <div className={styles.appContent}>
          {this.props.children}
        </div>

        {alert &&
        <Modal bsSize="small" show={!!alert} onHide={this.alertClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Error</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {alert.message}
          </Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Modal>}
      </div>
    );
  }
}
