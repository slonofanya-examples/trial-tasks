export default function login(req) {
  const name = req.body.name;

  if (name === 'wrong') {
    return Promise.reject({ error: 'Wrong user or password', status: 403 });
  }

  const user = { name };

  req.session.user = user;
  return Promise.resolve(user);
}
