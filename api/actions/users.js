const userList = ['firstUser', 'secondUser', 'thirdUser', 'fourthUser'];

export default function users() {
  return Promise.resolve(userList);
}
