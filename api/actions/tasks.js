import lodash from 'lodash'

let nextId = 10;
let taskList = [
  {
    id: 1,
    creationDate: 1463988886843,
    description: 'First task',
    owner: 'firstUser',
    assigned: 'firstUser',
    state: 'open'
  },
  {
    id: 2,
    creationDate: 1463988886843,
    description: 'Second task',
    owner: 'firstUser',
    assigned: 'firstUser',
    state: 'open'
  },
  {
    id: 3,
    creationDate: 1463988886843,
    description: 'Third task',
    owner: 'firstUser',
    assigned: 'firstUser',
    state: 'open'
  }
];

export default function tasks(req) {
  const description = req.body.description;
  const owner = req.session.user ? req.session.user.name : '';

  if (!owner) {
    return Promise.resolve([])
  }

  if (description) {
    const id = req.body.id;
    let newTask = {};

    if (id) {
      const index = lodash.findIndex(taskList, row => row.id === id);
      newTask = taskList[index] = {
        id,
        creationDate: taskList[index].creationDate,
        description,
        owner: taskList[index].owner,
        assigned: req.body.assigned ? req.body.assigned : 'firstUser',
        state: req.body.state ? req.body.state : 'open'
      };
    } else {
      newTask = {
        id: ++nextId,
        creationDate: Date.now(),
        description,
        owner,
        assigned: req.body.assigned ? req.body.assigned : 'firstUser',
        state: req.body.state ? req.body.state : 'open'
      };

      taskList.push(newTask);
    }

    return Promise.resolve(newTask);
  }

  if (req.method === 'DELETE') {
    const id = +req.query.id;
    taskList = lodash.filter(taskList, row => row.id !== id);
    return Promise.resolve({ delete: id })
  }

  return Promise.resolve(lodash.filter(taskList, task =>
    owner && (task.owner === owner || task.assigned === owner)
  ));
}
