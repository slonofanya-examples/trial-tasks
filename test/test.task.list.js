const Browser = require('zombie');

Browser.localhost('127.0.0.1', 3000);

describe('User visit home page', function() {
  this.timeout(20000);
  const browser = new Browser();

  before(function() {
    return browser.visit('/');
  });

  describe('submits form', function() {
    before(function() {
      return browser
        .fill('#username', 'firstUser')
        .fill('#password', 'password')
        .pressButton('.login-btn');
    });

    it('should see "Logged in" message', function() {
      browser.assert.text('.username', 'Logged in as firstUser.')
    });

    it('Tasks have to be appeared', function() {
      browser.assert.elements('tbody > tr', 3)
    });
  });
});
